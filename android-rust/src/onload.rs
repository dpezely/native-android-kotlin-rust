//! FIXME: This module is problematic!
//!
//! However, tests were successful on a physical device running
//! Android 4.2.2 (Samsung Galaxy Light, 2013) WITHOUT using
//! `JNI_OnLoad()`.  Therefore, This module should be UNNECESSARY
//! unless supporting obsolete versions of Android.
//!
//! Strictly speaking from point-of-view of Android NDK, the
//! `JNI_OnLoad()` function is optional, provided the native library
//! exposes its function with a particular signature.  That signature
//! involves a specific naming scheme, parameter sequence and return
//! type.
//!
//! That said, this module is here as jumping-off point for those so
//! interested.
//!
//! Further reading:
//!
//! https://developer.android.com/training/articles/perf-jni#native-libraries
//! https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/types.html
//! https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/functions.html#class_operations
//! https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/invocation.html#JNI_OnLoad

use jni::{JNIEnv, JNIVersion, JavaVM, NativeMethod};
use jni::strings::JNIString;
use jni::sys::{jint, JNI_ERR};
use std::ffi::c_void;

use crate::query;

const CALLING_CLASS: &str = "com/example/anagrams/ResultsViewModel";

// Called from Android Dalvik/Android Runtime to obtain native methods.
// This must exist UNLESS using precise name-mangling per JNI spec.
#[no_mangle]
#[allow(non_snake_case)]
pub extern "system"
fn JNI_OnLoad(runtime: *mut JavaVM, _reserved: *const c_void) -> jint {

    // FIXME: one of these `return JNI_ERR` statements gets executed
    // when running on emulator of "4_WVGA_Nexus_S_API_22" and crashes
    // elsewhere on 2013 era physical device.  No further explorations
    // were made, because our app works with the name-mangling
    // technique, as employed by `lib.rs` without this file.
    // We're not supporting anything older than Android 4.2.2 (API 17)
    // but preserve this code as-is in case it may help someone else.
    // Contributors welcome!

    let vm =
        if let Some(vm) = unsafe { runtime.as_ref() } {
            vm
        } else {
            return JNI_ERR;
        };
    let env: JNIEnv =
        if let Ok(env) = vm.get_env() {
            env
        } else {
            return JNI_ERR;
        };
    let class =
        if let Ok(c) = env.find_class(CALLING_CLASS) {
            c
        } else {
            return JNI_ERR;
        };
    let methods = [
        NativeMethod {
            name: JNIString::from("query"),
            // FIXME: this string's syntax is probably incorrect
            sig: JNIString::from("(Ljava/lang/Object;Ljava/lang/String)Ljava/lang/Object"),
            fn_ptr: query as *mut c_void
        },
    ];
    if env.register_native_methods(class, &methods).is_ok() {
        return JNIVersion::V6.into(); // the *minimum* JVM version required
    }
    return JNI_ERR;
}
