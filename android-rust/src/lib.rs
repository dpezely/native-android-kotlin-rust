//! Much of this code was adapted from anagram-phrases/src/bin/anagrams.rs
//! to acommodate populating an ArrayList via JNI for Android Runtime

#![cfg(target_os="android")]

extern crate serde_json;

use jni::JNIEnv;
use jni::objects::{JClass, JList, JObject, JString, JValue};
use jni::sys::jobject;
use std::collections::BTreeMap;
use std::convert::From;
use std::cmp;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::os::unix::io::FromRawFd;
use std::result::Result;

use anagram_phrases::languages::{self, Language, SHORT, UPCASE};
use anagram_phrases::primes::{self, Map};
use anagram_phrases::search::{self, Candidate};
use anagram_phrases::session::Session;

mod error;
mod log;
//mod onload;  // defines JNI_OnLoad() which calls register_native_methods()

use crate::error::ErrorKind;
use crate::log::{log_error, log_debug};

// Avoid overflowing Dalvik's local reference table (max=512) by using
// JNI::with_local_frame() peppered throughout construction of our
// nested Maps.
// However, as of Android 5.0 Lollipop (API level 21), Dalvik was
// replaced by the Android Runtime (ART), which makes Local Frames
// unecessary for most use cases.
// https://developer.android.com/training/articles/perf-jni#local-and-global-references
// https://android-developers.googleblog.com/2011/07/debugging-android-jni-with-checkjni.html
const LOCAL_FRAME_CAPACITY: usize = 512;

// Called from Android runtime by "discovery" of native methods.
#[no_mangle]
#[allow(non_snake_case)]
pub extern "system"
fn Java_com_example_anagrams_ResultsViewModel_query(
    env: JNIEnv, class: JClass, fd_obj: JObject, j_input: JString) -> jobject {

    query(env, class, fd_obj, j_input)
}

// Called from Android runtime by lookup.  Split `j_input` into query terms.
// If query is valid, call `search()`.  Otherwise, return empty `ArrayList`.
#[no_mangle]
#[allow(non_snake_case)]
pub extern "system"
fn query(env: JNIEnv, _class: JClass, fd_obj: JObject, j_input: JString) -> jobject {
    let input_string: String = get_j_string(&env, j_input);
    log_debug(format!("search: input_string={}", &input_string));
    if input_string.is_empty() {
        JObject::null().into_inner()
    } else {
        match search(&env, fd_obj, input_string) {
            Ok(obj) => obj.into_inner(),
            Err(e) => {
                // FIXME: .into() used for side-effect of consistent logging
                let _err: ErrorKind = e.into();
                JObject::null().into_inner()
            }
        }
    }
}

// Calls into Dalvik/ART for instantiating an ArrayList with multiple of
// elements of TreeMap.  Java considers these "global references"
// such that the structures outlive the "Native" call.
// See Java Reference docs regarding global and local references:
// https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/design.html#global_and_local_references
// For further development via the JNI crate, look at API docs for JNIEnv:
// https://docs.rs/jni/0.15.0/jni/struct.JNIEnv.html
// Lookups via env.new_object() are expensive, thus performed before
// the loop where possible.
//
// Results are contained within a Kotlin/Java MutableList for
// simplicity and ease of reading on the Dalvik/ART side of the JNI
// boundary while also considering Android-specific runtime issues.
//
// Because our library supports devices older than Android 8.0 (Oreo),
// "the number of local references is capped at a version-specific limit."
// Composing each MutableList is constrained by JNI frame size maximum
// of 512 local variable allocations, so truncation may occur.
fn search<'a>(env: &'a JNIEnv, fd_obj: JObject,
              query: String) -> Result<JList<'a,'a>, ErrorKind> {
    let file = open_file(&env, fd_obj)?;

    // FIXME: accommodate user config, and replace hard-coded values:
    // (See upstream library's command-line options in its src/bin/.)
    let session = Session::start(&Language::EN, vec![], false, 0,
                                 true, true, false, &query)?;

    let mut map: Map = BTreeMap::new();
    let mut wordlist: Vec<String> = vec![];
    // Add single word matches while loading dict:
    load_wordlist(&mut wordlist, &mut map, file, &session)?;
    let candidate = search::brute_force(&session.primes_product, &map,
                                        session.max_phrase_words);
    // Convert from differing enums:
    match package_results(&env, wordlist, candidate) {
        Ok(x) => Ok(x),
        Err(e) => Err(e.into())
    }
}

/// Load dictionary of natural language words (e.g., English) for
/// possible inclusion when searching combinations of words for
/// constructing candidate anagrams that match the input phrase.
///
/// Reject dictionary words based upon various criteria: 1) too long
/// to possibly match; 2) containing characters other than those from
/// the input pattern; 3) words where their product is greater than
/// that of the input phrase.
///
/// Params `phrases` is Java List of single word matches, and `map` is a
/// tree containing dictionary words selected after initial filtering.
/// Other parameters are same as their namesakes from
/// `primes::filter_word()`.
/// SIDE-EFFECTS: `wordlist` and `map` will be updated.
fn load_wordlist(wordlist: &mut Vec<String>, map: &mut Map, file: File,
                 session: &Session) -> Result<(), ErrorKind> {
    let input_length = session.essential.len();
    let empty: Vec<&str> = vec![];
    let short_words = SHORT.get(&session.lang).unwrap_or(&empty);
    let upcase_words = UPCASE.get(&session.lang).unwrap_or(&empty);
    let mut f = BufReader::new(file);
    let mut bytes: Vec<u8> = vec![];
    let mut word = String::new();
    let mut previous = String::new();
    let mut line = 0;
    loop {
        line += 1;
        bytes.clear();
        word.clear();
        match f.read_until(0x0A, &mut bytes) {
            Ok(0) => break,     // End of file (EOF)
            Ok(_n) => {
                if session.iso_8859_1 {
                    word = bytes.iter().map(|&x| char::from(x)).collect();
                } else {
                    word = String::from_utf8_lossy(&bytes).to_string();
                }
                word = word.trim().to_string();
                if word.is_empty() {
                    continue
                }
                if word == previous { // some dictionaries contain duplicates
                    continue
                }
                if languages::filter(&word, &short_words, &upcase_words,
                                     session.skip_short, session.skip_upcase) {
                    continue
                }
                if session.input_phrase.iter().any(|&x| x == word) {
                    continue    // filter words from input phrase
                }
                if let Ok(product) = primes::filter_word(&word, &session.pattern,
                                                         input_length,
                                                         &session.primes_product) {
                    if product == session.primes_product { // single word match
                        wordlist.push(word.to_string());
                    } else { // Store remaining words in look-up table:
                        map.entry(product)
                            .or_insert(Vec::with_capacity(1))
                            .push(word.to_string());
                    }
                }
                std::mem::swap(&mut previous, &mut word);
            }
            Err(e) => {
                log_error(format!("File error: line={} {:?}", line, e));
                break
            }
        }
    }
    Ok(())
}

fn open_file(env: &JNIEnv, fd_obj: JObject) -> Result<File, ErrorKind> {
    //let fd_class = env.find_class("java/io/FileDescriptor")?;
    let raw_fd: i32 = env.get_field(fd_obj, "descriptor", "I")?.i()?;
    let file = unsafe { File::from_raw_fd(raw_fd) };
    Ok(file)
}

// Arrange results within final ArrayList such that phrases are in
// ascending length: from single words to phrases with two words to
// three, etc.
//
// FIXME: if your Android app uses Android 5.0 Lollipop (API level 21)
// or newer, you may remove all statements mentioning local_frames here.
fn package_results<'a>(env: &'a JNIEnv, wordlist: Vec<String>,
                       results: Candidate) -> jni::errors::Result<JList<'a,'a>> {
    let results = results.0; // unpack tuple struct, Candidate
    let list_object = env.new_object("java/util/ArrayList", "(I)V",
                                     &[JValue::Int(results.len() as i32)])?;
    let phrases = JList::from_env(&env, list_object)?;

    let limit = cmp::min(wordlist.len(), LOCAL_FRAME_CAPACITY);
    env.push_local_frame(limit as i32)?;
    for word in &wordlist[0..limit] {
        let jstring = env.new_string(word)?;
        phrases.add(JObject::from(jstring))?;
    }
    env.pop_local_frame(JObject::null().into())?;
    let limit = cmp::min(results.len(), LOCAL_FRAME_CAPACITY);
    env.push_local_frame(limit as i32)?;
    let mut i = 0;
    for terms in &results {
        if terms.len() == 2 {
            let jstring = env.new_string(format!("{:?}", terms))?;
            phrases.add(JObject::from(jstring))?;
            i += 1;
            if i == LOCAL_FRAME_CAPACITY { break };
        }
    }
    env.pop_local_frame(JObject::null().into())?;
    env.push_local_frame(limit as i32)?;
    let mut i = 0;
    for terms in &results {
        if terms.len() == 3 {
            let jstring = env.new_string(format!("{:?}", terms))?;
            phrases.add(JObject::from(jstring))?;
            i += 1;
            if i == LOCAL_FRAME_CAPACITY { break };
        }
    }
    env.pop_local_frame(JObject::null().into())?;
    env.push_local_frame(limit as i32)?;
    let mut i = 0;
    for terms in &results {
        if terms.len() > 3 {
            let jstring = env.new_string(format!("{:?}", terms))?;
            phrases.add(JObject::from(jstring))?;
            i += 1;
            if i == LOCAL_FRAME_CAPACITY { break };
        }
    }
    env.pop_local_frame(JObject::null().into())?;
    Ok(phrases)
}

// Create Rust string from consumed Java String, or die trying
fn get_j_string(env: &JNIEnv, java_string: JString) -> String {
    env.get_string(java_string)
        .map_err(|e| {
            log_error(format!("Failed extracting String from Java: \
                               error=\"{:?}\"", e));
        })
        .unwrap()
        .into()
}
