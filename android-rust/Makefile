# Makefile for building Rust library for anagrams Android app

# Alternatively, add to your Android project's ./app/build.gradle
# file: under `android` section, add `externalNativeBuild` subsection.
# See also: https://developer.android.com/ndk/guides/android_mk

.PHONY: all
all: deps test release

ANDROID_NDK ?= /home/android-ndk

# If developing for this project:
.PHONY: developer
developer: deps
	@echo "Consider also running: make linux-deps"

# For Ubuntu 18.04 or OLDER, change `libvirt-daemon-system` to `libvirt-bin`
.PHONY: linux-deps
linux-deps:
	@echo "Invoking sudo for installing KVM and library dependencies"
	sudo apt-get install \
	  libssl-dev \
	  qemu-kvm libvirt-daemon-system bridge-utils \
	  libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386

# If you are deploying to a fresh workstation/laptop, start here.
# It is essentially https://www.rust-lang.org/en-US/install.html
# but modified with `-y` flag for full automation, plus goodies.
.PHONY: deps
deps:
	which rustc || \
	  (curl -sS --output /tmp/install-rust.sh https://sh.rustup.rs && \
	   /bin/bash /tmp/install-rust.sh -y)
	rustup component add rls clippy rust-analysis rust-src
	rustup target add aarch64-linux-android \
			armv7-linux-androideabi \
			x86_64-linux-android \
			i686-linux-android
	cargo install cargo-tree
	cargo install cargo-audit

.PHONY: update
update:
	rustup self update
	rustup update stable

# By default the Rust installation modifies PATH within ~/.profile
# but you may want this set within ~/.bashrc instead:
.PHONY: dot-bashrc
dot-bashrc:
	grep -c '\.cargo/bin' ~/.bashrc && \
	  (echo '~/.bashrc already configured' && false) || true
	@echo 'PATH="$${PATH}:$${HOME}/.cargo/bin"' >> ~/.bashrc
	@echo 'if [ $$(which rustc) ]; then'  >> ~/.bashrc
	@echo -n '   export RUST_SRC_PATH=' >> ~/.bashrc
	@echo '"$$(rustc --print sysroot)/lib/rustlib/src/rust/src"' >> ~/.bashrc
	@echo '   export RUST_BACKTRACE=1' >> ~/.bashrc
	@echo 'fi' >> ~/.bashrc
	@echo "Next, manually run: . ~/.bashrc"

# For generating `rustlib.so`, use the Android NDK toolchain.
# An architecture "triple" as environment variable identifer requires
# normalizing to underscores and upcase.
# These env vars avoid use of ~/.cargo/config file, thus less to maintain.
# https://doc.rust-lang.org/cargo/reference/config.html#targettriplelinker
# https://doc.rust-lang.org/cargo/reference/environment-variables.html#configuration-environment-variables
.PHONY: build
build:
	PATH=${PATH}:${ANDROID_NDK}/arm64/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER=bin/aarch64-linux-android-clang.sh \
	  cargo build --target aarch64-linux-android --release \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/arm/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER=bin/armv7-linux-androideabi-clang.sh \
	  cargo build --target armv7-linux-androideabi --release \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/x86/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_I686_LINUX_ANDROID_LINKER=bin/i686-linux-android-clang.sh \
	  cargo build --target i686-linux-android --release \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/x86_64/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER=bin/x86_64-linux-android-clang.sh \
	  cargo build --target x86_64-linux-android --release \
	    --features=embed-json

.PHONY: build-debug
build-debug:
	PATH=${PATH}:${ANDROID_NDK}/arm64/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER=bin/aarch64-linux-android-clang.sh \
	  cargo build --target aarch64-linux-android \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/arm/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER=bin/armv7-linux-androideabi-clang.sh \
	  cargo build --target armv7-linux-androideabi \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/x86/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_I686_LINUX_ANDROID_LINKER=bin/i686-linux-android-clang.sh \
	  cargo build --target i686-linux-android \
	    --features=embed-json
	PATH=${PATH}:${ANDROID_NDK}/x86_64/bin \
	ANDROID_NDK=${ANDROID_NDK} \
	CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER=bin/x86_64-linux-android-clang.sh \
	  cargo build --target x86_64-linux-android \
	    --features=embed-json

.PHONY: test
test:
	cargo test

.PHONY: audit
audit:
	cargo audit

.PHONY: release
release: clean-build build
	ls -lh target/*-linux-android*/release/*.so

.PHONY: clean-build
clean-build:
	cargo clean --release -p $(shell cargo pkgid)

.PHONY: clean-lib
clean-lib:
	find target/ -name librust.so -delete

.PHONY: clean
clean:
	cargo clean || true

.PHONY: dist-clean
dist-clean: clean
	find . -name '*~' -delete
	rm -f Cargo.lock
