package com.example.anagrams

// Signature of results from Rust library via Native call
typealias Resultset = List<String>
