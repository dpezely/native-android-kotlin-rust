Native Android App Using Rust + Kotlin
======================================

This is the Android specific portion of a Rust-based mobile app.

The [Rust programming language](https://rust-lang.org/) implements primary
business logic, and [Kotlin programming language](https://kotlinlang.org/)
handles only User Interface behaviour.

It performs well on a physical device running Android 4.2.2
(Jelly Bean; API level 17; Samsung Galaxy Light, early 2013)
as well as an x86 emulator of "Nexus_4_API_17" representing the same era.
These older 4.2.x versions were observed on
[about 1.5%](https://www.androidauthority.com/android-version-distribution-748439/)
of devices as reported May 2019.

At time of writing (2020-03-22) Google
[requires supporting](https://support.google.com/googleplay/android-developer/answer/113469#targetsdk)
Android 8.0 (API level 26) or
[newer](https://developer.android.com/distribute/best-practices/develop/target-sdk).

## Overview

- Rust (rustc 1.42)
- Architecture triples:
  + 64-bit ARM: aarch64-linux-android
  + 32-bit ARM: armv7-linux-androideabi
  + 32-bit x86: i686-linux-android
  + 64-bit x86: x86_64-linux-android
- Linkage via `clang` from Android SDK standalone toolchain
  + with `HASH_STYLE` set to `both` GNU and SYSV for legacy support
- Compilation work-flow via Makefile which dispatches `gradlew`
- Updates via `sdkmanager` for compatibility with Android Studio
- Kotlin v1.3
- Android `buildToolsVersion` matches `targetSdkVersion`
- Android `targetSdkVersion` latest available at time of release
- Android `minSdkVersion` 17 for accommodating Android 4.2
- Android NDK `ndkVersion` latest available at time of release
- Android NDK toolchain compatibility matches `minSdkVersion`
- Two activities/screens: query, results
- RecyclerView and ViewModel for results activity
- JNI called from Rust to push/pop Local Frame for accommodating older Dalvik Runtime
  + Optional/unnecessary for newer Android Runtime (ART) which is the
    default runtime for devices running Android 5.0 Lollipop (API level 21)
- Single "universal" .apk file, ensuring shared library exists
  + See `app/build.gradle` for example `splits` to per-architecture .apk files

To Do:

- Add search query to `ResultsActivity` Fragment
- Add a `Service` so that other apps may initiate a query
- Add a "Settings" menu
- Add "bookmarks" or other means by which to preserve result subsets of interest
- Add invoke `Intent` of `ACTION_SEND` to "share on social media"
- Further automate `make update` to extract latest *stable* version numbers
  from `sdkmanager --list`

Contributors welcome!

## Relevant Files

Familiar to seasoned Android developers:

- [app/build.gradle](app/build.gradle) - specify `targetSdkVersion` and `ndkVersion`
- [app/src/main/AndroidManifest.xml](app/src/main/AndroidManifest.xml)
- [app/src/main/java/com/example/anagrams/*.kt](app/src/main/java/com/example/anagrams/)
- [app/src/main/res/layout/*.xml](app/src/main/res/layout/)
- [app/src/main/res/values/strings.xml](app/src/main/res/values/strings.xml)

Building and running entirely via shell command-line:

- [Makefile](./Makefile)
- [app-sdk-requirements.txt](app-sdk-requirements.txt) - for updates
- [vm.csv](vm.csv) - list of devices to emulate for local testing

See also:
[android-makefile-without-ide](https://gitlab.com/dpezely/android-makefile-without-ide)

## Background

See also: [android-rust](../android-rust/)

The goal of this project was to provide an end-to-end example of Android
Native development using Kotlin and Rust.

Build, test, emulate, deploy and SDK update work-flows may be controlled
from `Makefile`s.

Android Studio is used here primarily for its Java runtime and Kotlin
compiler, thereby *ensuring compatibility and compliance.*

This project started from reading the
[Rust on Android](https://medium.com/visly/rust-on-android-19f34a2fb43) blog
post, meaning zero working knowledge of Kotlin or Android development yet
being a seasoned Rust developer.

Since their post, Google officially recommends Kotlin for all new apps, and
use `androidx`.

This project represents results of that learning curve while attempting to
adhere to Android UI/UX "best practices".  These include using a
`RecyclerView` and `ViewModel` within Kotlin code and proper `JNI`
invocations from Rust for seamless use cases within Kotlin.

Caveats:

- The native shared library's handling of the dictionary word-list file most
  certainly *violates* Android "best practices" for the current version due
  to learning curve, time available for research, etc.
- Currently using [AssetFileDescriptor](https://groups.google.com/d/msg/android-ndk/H4P0SAkzMsM/Ddt8HiA7MvMJ)
  from [2010](https://groups.google.com/forum/#!topic/android-ndk/H4P0SAkzMsM)
- Look for "FIXME" or Debian/Ubuntu specific bits in Makefile
      
## Setup

First, ensure sufficient storage capacity is available on your
laptop/workstation and ability to download the following quantities.

Most downloads are via `make` below.

Example disk usage after several weeks and upgrading the IDE:

     1.4G	/home/android-studio/
    13  G	/home/android-sdk/
     9  G	/home/android-ndk/
     0.8G	~/.AndroidStudio3.5/
     0.4G	~/.AndroidStudio3.6/
     7.5G	~/.android/
     2.4G	~/.gradle/
    -------------
    34.5G	total

On Linux, enable KVM for emulator of Android devices.

The "kvm" group enables access to `/dev/kvm` for running virtual
machines:

    sudo usermod -G kvm -a $USER

- https://help.ubuntu.com/community/KVM/Installation
  + You must **logout and login again** for new groups to take effect

Download Android Studio, and its `sdkmanager` utility invoked from our
[Makefile](./Makefile) *should* handle the rest-- until Google changes
something again.

- https://developer.android.com/studio/
- https://developer.android.com/studio/install
- https://developer.android.com/studio/releases/build-tools
- https://developer.android.com/reference/android/os/Build.VERSION_CODES#R

Important environment variables will be defined along the way and used
further below...

Extract:

    ANDROID_STUDIO=/home/android-studio
    sudo mkdir -p $ANDROID_STUDIO
    sudo chown $USER.$USER $ANDROID_STUDIO
	tar zxf ~/Downloads/android-studio-ide-*.tar.gz -C $ANDROID_STUDIO

In preparation for installing the SDK and NDK, consider that these paths
**must be outside** of where Android Studio has been installed.  Create
`/home/android-sdk/`.

Create subdirectories:

    ANDROID_SDK=/home/android-sdk
    sudo mkdir -p $ANDROID_SDK
    sudo chown $USER.$USER $ANDROID_SDK

    ANDROID_NDK=/home/android-ndk
    sudo mkdir -p $ANDROID_NDK
    sudo chown $USER.$USER $ANDROID_NDK

Highly recommended but optional: due to several gigabytes and several
hundred megabytes respectively, consider making `~/.android` and
`~/.AndroidStudio*` as sym-links pointing outside of $HOME.

    sudo mkdir /home/android-studio3.6_$USER
    sudo chown $USER.$USER !$
    ln -s !$ ~/.AndroidStudio3.6

    sudo mkdir /home/android-studio_$USER.android
    sudo chown $USER.$USER !$
    ln -s !$ ~/.android

    sudo mkdir /home/android-studio_$USER.gradle
    sudo chown $USER.$USER !$
    ln -s !$ ~/.gradle

Use Android Studio to install only basic components, and the rest will be
installed in bulk below (allowing time to watch a long movie or two).

- When prompted, use *your paths* from above
- From Android-Studio IDE:
  + Build menu -> Android SDK from Left panel -> SDK Tools tab
  + Select "Android SDK Build-Tools"
  + Select "Android SDK Platform-Tools"
  + Select "Android SDK Tools"
- Omit `NDK` and `Emulator`, as these will be fetched below.
- Omitting `CMake` is fine, as Rust's `cargo` will handle that functionality.
- Including `LLDB` is optional for debugging but beyond scope here.
- Other items such system images will be fetched below.

> Optional details on installing the NDK:
>
> - https://developer.android.com/ndk/
> - https://developer.android.com/ndk/guides#download-ndk
> - https://developer.android.com/ndk/downloads/index.html

The Makefile uses the the SDK's `sdkmanager` command-line utility.

Run as **one** command:

    ANDROID_STUDIO=$ANDROID_STUDIO \
      ANDROID_SDK=$ANDROID_SDK \
      ANDROID_NDK=$ANDROID_NDK \
      make deps

Consider editing [Makefile](./Makefile) and [../Makefile](../Makefile),
setting those variables to accommodate paths on your laptop/workstation
for future use.

> Optionally, build a first app.  Consider these tutorials:
>
> - https://developer.android.com/training/basics/firstapp/
> - https://www.udacity.com/course/new-android-fundamentals--ud851
> - https://developer.android.com/studio/intro/
> - https://github.com/mozilla/rust-android-gradle

Build the dependent library within the larger project:

    (cd ../android-rust/ && make)

Build this app:

    make import build

For running on an emulator:

    make new-emulators
    make emulator DEVICE_NAME=Nexus_9_API_28 &
    make run

For running on a physical device attached via USB cable:

    make deploy

From separate shells, check logs:
(assuming either emulator *or* device is used, not both)

    make log
    make app-log

For the final .apk, building the release also performs lint checks:

    make release

To optionally expunge this app from the emulator or device:
(assuming either emulator *or* device is used, not both)

    make uninstall


## Staying Current

Stay current with latest Android Studio, SDK, NDK and API Levels.  At least
one of these is likely to change every few weeks or months.

For an equivalent to updating within Android Studio:

- i.e., "Help" menu -> "Check for Updates"

Instead, you may use `sdkmanager` from the command-line:

- Run `make check-versions` (Lots of output!)
- Revise [app-sdk-requirements.txt](./app-sdk-requirements.txt)
  + Some items need a version, such as `ndk;21.0.6113669`
  + Others may omit a version, such as `ndk-bundle`
  + The SDK itself is `platforms;android-29`
- Run `make update` to fetch based upon that requirements file.
- Maybe run `make new-emulators` after editing [vm.csv](./vm.csv) to account for:
  + new API levels
  + [new devices](https://www.devicespecifications.com/); see also:
    [Samsung skins](https://developer.samsung.com/galaxy-emulator-skin/overview.html)
    which may be installed into
    `${ANDROID_STUDIO}/plugins/android/lib/device-art-resources/`

The `app-sdk-requirements.txt` file included with this project covers
dependencies dating back to Android 4.x (even though Google only
*officially* supports Android 8.0 and higher at time of writing, and Android
6.0 is *unofficially* accommodated).

When pruning that file-- such as when Google officially deprecates or
obsoletes old Android API levels-- remove lines corresponding with the
outdated system images to a separate file, and run:

    JAVA_HOME=/home/android-studio/jre \
      /home/android-sdk/tools/bin/sdkmanager --uninstall OBSOLETE.txt

Paths on your host laptop/workstation may differ from the above example.

Periodically, confirm minimum and target Android API levels within
[app/build.gradle](app/build.gradle) file.

Values in that file are used by [Makefile](./Makefile) to specify the
NDK API version such that it
[matches](https://github.com/aosp-mirror/platform_bionic/blob/master/android-changes-for-ndk-developers.md#__register_atfork-available-in-api-level--23)
`minSdkVersion`.

If `minSdkVersion` changed, re-construct the standalone toolchain from the
prebuilt SDK ndk-bundle:

    make android-ndk-deps

Finally, recompile:

    (cd ../android-rust/ && make)
    make import release

## Addendum

Highlights from the
[androiddev subreddit](https://www.reddit.com/r/androiddev/comments/g1k5zh/native_android_app_with_kotlin_and_rust_code/)
discussion:

"Some android-y stuff could be cleaned up a bit but I feel like that's not
the focus in this achievement." --[u/Zhuinden](https://www.reddit.com/user/Zhuinden/)

## License

[MIT License](../LICENSE)

Essentially, do as you wish and without warranty from authors or maintainers.
